# DocWork
DocWork is an API to access document files while hidding the real type of the container.

#Overview
This API allows to get access to text files content. For the sake of simplicity, all the text is supposed to use the same font and size.
Documents are split into paragraphs, which are then divided in Spans.

# Classes

## DocWorkFile
This is the main class of the API, containing several methods to manage the file.

It has static openers for ODT and DOCX files :

`public static final DocWorkFile loadDocWorkFile(String path)`

`public static final DocWorkFile loadDocWorkFile(File inputfile)`

It offers a way to get the content and the size in characters of the file :

`public List<DocWorkParagraph> getParagraphs();`

`public int calculateSize();`

It has also ways to update the content :

`public void appendParagraph(String text)`

` public void appendParagraph(DocWorkParagraph paragraph)`

It also offers a way to save the modifications :
`public void save()`

## DocWorkParagraph
A paragraph is a bunch of text pans put in a single pack, with a common alignement. A paragraph may be set as starting on a new page or not.
The methods are :

`public List<DocWorkSpan> getContent()`

`public void add(DocWorkSpan span)`

`public Alignment getAlignment()`

`public void setAlignment(Alignment alignment)`

`public boolean isPageBreak()`

`public void setPageBreak(boolean pageBreak)`

## DocWorkSpan
A span is a part of text in a paragraph that shares the same characteristics (bold and italic).
The constructor is :

`public DocWorkSpan(String content, boolean bold, boolean italic)`

The methods are :

`public String getContent()`

`public void setContent(String content)`

`public boolean isBold()`

`public void setBold(boolean bold)`

`public boolean isItalic()`

`public void setItalic(boolean italic)`
