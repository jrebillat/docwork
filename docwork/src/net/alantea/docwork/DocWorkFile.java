package net.alantea.docwork;

import java.io.File;
import java.util.List;

import net.alantea.docwork.docx.DocxFile;
import net.alantea.docwork.odt.OdtFile;


/**
 * The Class DocWorkFile.
 */
public abstract class DocWorkFile
{
   
   /**
    * Load doc work file.
    *
    * @param path the path
    * @return the doc work file
    */
   public static final DocWorkFile loadDocWorkFile(String path)
   {
      return loadDocWorkFile(new File(path));
   }
   

   /**
    * Load doc work file.
    *
    * @param inputfile the input file
    * @return the doc work file
    */
   public static final DocWorkFile loadDocWorkFile(File inputfile)
   {
      if (inputfile.getName().endsWith(".docx"))
      {
         return new DocxFile(inputfile);
      }
      else if (inputfile.getName().endsWith(".odt"))
      {
         return new OdtFile(inputfile);
      }
      return null;
   }
   
   /**
    * Save.
    */
   public abstract void save();
   
   /**
    * Append part.
    *
    * @param file the file 
    */
   public void appendPart(DocWorkFile file)
   {
      List<DocWorkParagraph> paras = file.getParagraphs();
      for (DocWorkParagraph para : paras)
      {
         appendParagraph(para);
      }
   }
   
   /**
    * Append paragraph.
    *
    * @param text the text
    */
   public abstract void appendParagraph(String text);
   
   /**
    * Append paragraph.
    *
    * @param paragraph the paragraph
    */
   public abstract void appendParagraph(DocWorkParagraph paragraph);

   /**
    * Calculate size.
    *
    * @return the size in characters including spaces
    */
   public abstract int calculateSize();
   
   /**
    * Gets the paragraphs.
    *
    * @return the paragraphs
    */
   public abstract List<DocWorkParagraph> getParagraphs();
   
   /**
    * Gets the complete text content.
    *
    * @return the text content
    */
   public abstract String getTextContent();

   /**
    * Close file.
    */
   public abstract void close();


   /**
    * Gets the text.
    *
    * @return the text
    */
   public String getText()
   {
      String ret = "";
      for (DocWorkParagraph paragraph : getParagraphs())
      {
         ret += paragraph.getText() + "\n";
      }
      return ret;
   }

}
