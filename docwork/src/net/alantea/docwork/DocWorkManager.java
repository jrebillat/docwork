package net.alantea.docwork;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


/**
 * The Class DocWorkFile.
 */
public abstract class DocWorkManager
{
   /** The mapper. */
   private static Map<String, DocWorkManager> mapper = new HashMap<>();
   
   /** The loaded files. */
   private static Map<String, DocWorkFile> loadedFiles = new HashMap<>();
   
   /**
    * Instantiates a new doc work manager.
    *
    * @param extension the extension
    */
   protected DocWorkManager(String extension)
   {
      mapper.put(extension, this);
   }
   
   /**
    * Load doc work file.
    *
    * @param path the path
    * @return the doc work file
    */
   public static final DocWorkFile loadDocWorkFile(String path)
   {
      DocWorkFile ret = loadedFiles.get(path);
      if (ret == null)
      {
         File inputfile = new File(path);
         for (String key : mapper.keySet())
         {
            if (inputfile.getName().endsWith(key))
            {
               ret = mapper.get(key).getDocWorkFile(inputfile);
            }
         }
      }
      return ret;
   }
   
   /**
    * Gets the characters count.
    *
    * @param path the path
    * @return the count
    */
   public static int getSize(String path)
   {
      DocWorkFile docFile = loadDocWorkFile(path);
      if (docFile != null)
      {
         return docFile.calculateSize();
      }
      return 0;
   }
   
   /**
    * Gets the doc work file.
    *
    * @param inputfile the input file to parse
    * @return the doc work file or null
    */
   protected abstract DocWorkFile getDocWorkFile(File inputfile);
}
