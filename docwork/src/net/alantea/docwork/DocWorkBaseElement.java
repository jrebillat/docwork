package net.alantea.docwork;

import java.awt.Color;

/**
 * The Class DocWorkParagraph.
 */
public class DocWorkBaseElement
{
   /** Is it bold. */
   private boolean bold;
   
   /** Is it italic. */
   private boolean italic;
   
   private double fontSize;
   
   private String fontFamily;
   
   private Color fontColor;
   
   private Color backgroundColor;

   /**
    * Instantiates a new doc work paragraph.
    */
   public DocWorkBaseElement()
   {
      bold = false;
      italic = false;
      fontSize = 12.0;
   }

   /**
    * Checks if it is bold.
    *
    * @return true, if is bold
    */
   public boolean isBold()
   {
      return bold;
   }

   /**
    * Sets the bold flag.
    *
    * @param bold the new bold
    */
   public void setBold(boolean bold)
   {
      this.bold = bold;
   }

   /**
    * Checks if it is italic.
    *
    * @return true, if is italic
    */
   public boolean isItalic()
   {
      return italic;
   }

   /**
    * Sets the italic flag.
    *
    * @param italic the new italic
    */
   public void setItalic(boolean italic)
   {
      this.italic = italic;
   }

   public double getFontSize()
   {
      return fontSize;
   }

   public void setFontSize(double size)
   {
      if (size > 0.0)
      {
         fontSize = size;
      }
   }

   public String getFontFamily()
   {
      return fontFamily;
   }

   public void setFontFamily(String fontFamily)
   {
      this.fontFamily = fontFamily;
   }

   public Color getFontColor()
   {
      return fontColor;
   }

   public void setFontColor(Color fontColor)
   {
      this.fontColor = fontColor;
   }

   public Color getBackgroundColor()
   {
      return backgroundColor;
   }

   public void setBackgroundColor(Color backgroundColor)
   {
      this.backgroundColor = backgroundColor;
   }
}
