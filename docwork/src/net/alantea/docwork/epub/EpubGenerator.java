package net.alantea.docwork.epub;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import nl.siegmann.epublib.domain.Author;
import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.MediaType;
import nl.siegmann.epublib.domain.Metadata;
import nl.siegmann.epublib.domain.Resource;
import nl.siegmann.epublib.epub.EpubWriter;

public class EpubGenerator
{
   Book book;
   Metadata metadata;

   public EpubGenerator(String title, String author)
   {
      book = new Book();
      metadata = book.getMetadata();
      metadata.addTitle(title);
      metadata.addAuthor(new Author(author));
   }
   
   public void write(String path) throws FileNotFoundException, IOException
   {
      write(new File(path));
   }
   
   public void write(File file) throws FileNotFoundException, IOException
   {
      EpubWriter epubWriter = new EpubWriter();
      epubWriter.write(book, new FileOutputStream(file));
      
   }

   public void addStringSection(String title, String content ) throws IOException
   {
      Resource res = new Resource(title);
      res.setMediaType(new MediaType("text/plain", "txt"));
      res.setData(content.getBytes());
      book.addSection(title, res);
   }

   public void addHtmlSection(String title, String content ) throws IOException
   {
      Resource res = new Resource(title);
      res.setMediaType(new MediaType("text/html", "html"));
      res.setData(content.getBytes());
      book.addSection(title, res);
   }
   
   public void setCss(String content)
   {
      Resource res = new Resource("css");
      res.setMediaType(new MediaType("text/css", "css"));
      res.setData(content.getBytes());
      book.getResources().add(res);
   }
}
