package net.alantea.docwork;

import java.util.LinkedList;
import java.util.List;

/**
 * The Class DocWorkParagraph.
 */
public class DocWorkParagraph extends DocWorkBaseElement
{
   
   /**
    * The Enum Alignment.
    */
   public static enum Alignment 
   {
      
      /** The left. */
      LEFT,
      
      /** The center. */
      CENTER,
      
      /** The right. */
      RIGHT,
      
      /** The justified. */
      JUSTIFIED
   }

   /** The alignment. */
   private Alignment alignment = Alignment.LEFT;

   /** The content. */
   private List<DocWorkSpan> content = new LinkedList<>();

   /** Is it a page break. */
   private boolean pageBreak;
   
   private int headingLevel = 0;
   /* 0 means paragraph text */

   /**
    * Instantiates a new doc work paragraph.
    */
   public DocWorkParagraph()
   {
      pageBreak = false;
   }

   /**
    * Gets the alignment.
    *
    * @return the alignment
    */
   public Alignment getAlignment()
   {
      return alignment;
   }

   /**
    * Sets the alignment.
    *
    * @param alignment the new alignment
    */
   public void setAlignment(Alignment alignment)
   {
      this.alignment = alignment;
   }

   /**
    * Gets the content.
    *
    * @return the content
    */
   public List<DocWorkSpan> getContent()
   {
      return content;
   }

   /**
    * Adds the span element.
    *
    * @param span the span to add
    */
   public void add(DocWorkSpan span)
   {
      this.content.add(span);
   }

   /**
    * Gets the text.
    *
    * @return the text
    */
   public String getText()
   {
      String ret = "";
      for (DocWorkSpan span : content)
      {
         ret += span.getContent();
      }
      return ret;
   }

   /**
    * Checks if it is page break.
    *
    * @return true, if is page break
    */
   public boolean isPageBreak()
   {
      return pageBreak;
   }

   /**
    * Sets the page break.
    *
    * @param pageBreak the new page break
    */
   public void setPageBreak(boolean pageBreak)
   {
      this.pageBreak = pageBreak;
   }

   /**
    * Gets the heading level.
    *
    * @return the heading level
    */
   public int getHeadingLevel()
   {
      return headingLevel;
   }

   /**
    * Sets the heading level.
    *
    * @param headingLevel the new heading level
    */
   public void setHeadingLevel(int headingLevel)
   {
      this.headingLevel = headingLevel;
   }
}
