package net.alantea.docwork.odt;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.odftoolkit.odfdom.dom.element.text.TextParagraphElementBase;
import org.odftoolkit.odfdom.dom.element.text.TextSpanElement;
import org.odftoolkit.odfdom.dom.style.props.OdfTextProperties;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;
import org.odftoolkit.odfdom.type.Color;
import org.odftoolkit.simple.Document;
import org.odftoolkit.simple.Document.ScriptType;
import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.style.DefaultStyleHandler;
import org.odftoolkit.simple.style.Font;
import org.odftoolkit.simple.style.StyleTypeDefinitions;
import org.odftoolkit.simple.style.StyleTypeDefinitions.FontStyle;
import org.odftoolkit.simple.style.StyleTypeDefinitions.HorizontalAlignmentType;
import org.odftoolkit.simple.style.StyleTypeDefinitions.TextLinePosition;
import org.odftoolkit.simple.style.TextProperties;
import org.odftoolkit.simple.text.Paragraph;
import org.odftoolkit.simple.text.ParagraphStyleHandler;
import org.odftoolkit.simple.text.Span;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.alantea.docwork.DocWorkFile;
import net.alantea.docwork.DocWorkParagraph;
import net.alantea.docwork.DocWorkSpan;
import net.alantea.docwork.DocWorkParagraph.Alignment;
import net.alantea.utils.exception.LntException;

/**
 * The Class OdtFile.
 */
public class OdtFile extends DocWorkFile
{
   /** The target file. */
   private File targetFile;
   
   /** The text document. */
   private TextDocument textDocument;
   
   /**
    * Instantiates a new odt file.
    *
    * @param path the path
    */
   public OdtFile(String path)
   {
      this(new File(path));
   }
   

   /**
    * Instantiates a new odt file.
    *
    * @param inputfile the inputfile
    */
   public OdtFile(File inputfile)
   {
      try
      {
         if (inputfile.exists())
         {
            textDocument = TextDocument.loadDocument(inputfile);
         }
         else
         {
            textDocument = TextDocument.newTextDocument();
         }
         targetFile = inputfile;
      }
      catch (Exception e)
      {
         new LntException("Error creating ODT file information class : " + inputfile.getAbsolutePath(), e);
      }
   }

   /**
    * Save.
    */
   @Override
   public void save()
   {
      try
      {
         textDocument.save(targetFile);
      }
      catch (Exception e)
      {
         new LntException("Error saving ODT file : " + targetFile.getAbsolutePath(), e);
      }
   }

   /**
    * Close.
    */
   @Override
   public void close()
   {
      textDocument.close();
   }

   /**
    * Append paragraph.
    *
    * @param text the text
    */
   @Override
   public void appendParagraph(String text)
   {
      textDocument.addParagraph(text);
   }

   /**
    * Append paragraph.
    *
    * @param para the para
    */
   @Override
   public void appendParagraph(DocWorkParagraph para)
   {      
      if (para.isPageBreak())
      {
         textDocument.addPageBreak();
      }
      
      Paragraph paragraph = textDocument.addParagraph("");
      StyleTypeDefinitions.FontStyle style = StyleTypeDefinitions.FontStyle.REGULAR;
      if ((para.isBold()) && (para.isItalic()))
      {
         style = StyleTypeDefinitions.FontStyle.BOLDITALIC;
      }
      else if (para.isBold())
      {
         style = StyleTypeDefinitions.FontStyle.BOLD;
      }
      else if (para.isItalic())
      {
         style = StyleTypeDefinitions.FontStyle.ITALIC;
      }
      Font origfont = paragraph.getFont();
      Font font = new Font(
            (para.getFontFamily() != null) ? para.getFontFamily() : origfont.getFamilyName(),
                  style, para.getFontSize());
      if (para.getFontColor() != null)
      {
         font.setColor(new Color(para.getFontColor()));
      }
      if (para.getBackgroundColor() != null)
      {
         paragraph.getStyleHandler().getTextPropertiesForWrite().setBackgroundColorAttribute(new Color(para.getBackgroundColor()));
      }
      paragraph.setFont(font);
      int headingLevel = para.getHeadingLevel();
      paragraph.applyHeading(headingLevel != 0, headingLevel);
      if (headingLevel == 0)
      {
         Alignment align = para.getAlignment();
         switch(align)
         {
            case RIGHT :
               paragraph.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);
               break;
            case CENTER :
               paragraph.setHorizontalAlignment(HorizontalAlignmentType.CENTER);
               break;
            case JUSTIFIED :
               paragraph.setHorizontalAlignment(HorizontalAlignmentType.JUSTIFY);
               break;
            default :
               paragraph.setHorizontalAlignment(HorizontalAlignmentType.LEFT);
               break;
         }
         
         for (DocWorkSpan docspan : para.getContent())
         {
            TextParagraphElementBase node = paragraph.getOdfElement();
            TextSpanElement spanElement;
            try
            {
               spanElement = textDocument.getContentDom().newOdfElement(TextSpanElement.class);
               Span span = Span.getInstanceof(spanElement);
               OdfStyle spanStyle = (OdfStyle) spanElement.getOrCreateUnqiueAutomaticStyle();
               if (docspan.isItalic())
               {
                  spanStyle.setProperty(OdfTextProperties.FontStyle, "italic");
               }
               if (docspan.isBold())
               {
                  spanStyle.setProperty(OdfTextProperties.FontWeight, "bold");
               }
               if (docspan.getFontColor() != null)
               {
                  span.getStyleHandler().getTextPropertiesForWrite().setFontColor(new Color(docspan.getFontColor()));
               }
               if (docspan.getBackgroundColor() != null)
               {
                  span.getStyleHandler().getTextPropertiesForWrite().setBackgroundColorAttribute(new Color(docspan.getBackgroundColor()));
               }
               spanElement.setTextContent(docspan.getContent());
               node.appendChild(spanElement);
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
         }
      }
      else
      {
         String txt = "";
         for (DocWorkSpan docspan : para.getContent())
         {
            txt += docspan.getContent();
         }
         paragraph.setTextContent(txt);
         String stylename = "Heading_20_" + headingLevel;
         paragraph.getOdfElement().setStyleName(stylename);
      }
   }

   /**
    * Calculate size.
    *
    * @return the int
    */
   @Override
   public int calculateSize()
   {
      try
      {
         int size = 0;
         TextDocument doc = TextDocument.loadDocument(targetFile);
         Iterator<Paragraph> iter = doc.getParagraphIterator();
         while (iter.hasNext())
         {
            Paragraph para = iter.next();
            String content = para.getTextContent();
            size += content.length();
         }
         return size;
      }
      catch (Exception e)
      {
         new LntException("Error getting ODT file informations : " + targetFile.getAbsolutePath(), e);
      }
      return -1;
   }


   /**
    * Gets the paragraphs.
    *
    * @return the paragraphs
    */
   @Override
   public List<DocWorkParagraph> getParagraphs()
   {
      List<DocWorkParagraph> ret = new LinkedList<>();
      Iterator<Paragraph> iter = textDocument.getParagraphIterator();
      while (iter.hasNext())
      {
         Paragraph para = iter.next();
         DocWorkParagraph paragraph = new DocWorkParagraph();
         try
         {
            int headingLevel = para.getHeadingLevel();
            paragraph.setHeadingLevel(para.getHeadingLevel());

            boolean parabold = false;
            boolean paraitalic = false;
            Color paracolor = null;
            Color paraBackColor = null;

            if (headingLevel == 0)
            {
               try
               {
                  Font parafont = getFont(para);
                  paracolor = parafont.getColor();
                  paracolor = parafont.getColor();
                  paragraph.setFontSize(parafont.getSize());
                  paragraph.setFontFamily(parafont.getFamilyName());
                  paragraph.setFontColor(parafont.getColor().getAWTColor());
                  String paraBackColorStr = para.getStyleHandler().getTextPropertiesForRead().getBackgroundColorAttribute();
                  if (paraBackColorStr != null)
                  {
                     paraBackColor = Color.valueOf(paraBackColorStr);
                  }
                  if (parafont != null)
                  {
                     FontStyle style = parafont.getFontStyle();
                     switch( style)
                     {
                        case BOLD :
                           parabold = true;
                           break;
                        case ITALIC :
                           paraitalic = true;
                           break;
                        case BOLDITALIC :
                           parabold = true;
                           paraitalic = true;
                           break;
                        default :
                           break;
                     }
                  }
               }
               catch (Exception e)
               {
               }
               paragraph.setBold(parabold);
               paragraph.setItalic(paraitalic);

               NodeList nodes = para.getOdfElement().getChildNodes();
               for (int i = 0; i < nodes.getLength(); i++)
               {
                  Node n = nodes.item(i);
                  if (n instanceof TextSpanElement)
                  {
                     boolean bold = parabold;
                     boolean italic = paraitalic;
                     TextSpanElement spanNode =(TextSpanElement) n;
                     DefaultStyleHandler styleHandler = new DefaultStyleHandler(spanNode);
                     TextProperties props = styleHandler.getTextPropertiesForRead();
                     Color color = null;
                     Color backColor = null;
                     if (props != null)
                     {
                        Font font = props.getFont();
                        color = props.getFontColor();
                        String backColorStr = props.getBackgroundColorAttribute();
                        if (backColorStr != null)
                        {
                           backColor = Color.valueOf(backColorStr);
                        }
                        FontStyle style = font.getFontStyle();
                        switch (style)
                        {
                           case BOLD :
                              bold = true;
                              break;

                           case ITALIC :
                              italic = true;
                              break;

                           case BOLDITALIC :
                              bold = true;
                              italic = true;
                              break;

                           default :
                              break;

                        }
                     }
                     DocWorkSpan span = new DocWorkSpan(n.getTextContent());
                     span.setBold(bold);
                     span.setItalic(italic);
                     if (color != null)
                     {
                        span.setFontColor(color.getAWTColor());
                     }
                     else if (paracolor != null)
                     {
                        span.setFontColor(paracolor.getAWTColor());
                     }
                     if (backColor != null)
                     {
                        span.setBackgroundColor(backColor.getAWTColor());
                     }
                     else if (paraBackColor != null)
                     {
                        span.setBackgroundColor(paraBackColor.getAWTColor());
                     }
                     paragraph.add(span);
                  }
                  else
                  {
                     DocWorkSpan span = new DocWorkSpan(n.getTextContent());
                     span.setBold(parabold);
                     span.setItalic(paraitalic);
                     if (paracolor != null)
                     {
                        span.setFontColor(paracolor.getAWTColor());
                     }
                     if (paraBackColor != null)
                     {
                        span.setBackgroundColor(paraBackColor.getAWTColor());
                     }
                     paragraph.add(span);

                  }
               }
            }
            else
            {
               DocWorkSpan span = new DocWorkSpan(para.getTextContent());
               paragraph.add(span);
            }
         }
         catch (NullPointerException e)
         {
            e.printStackTrace();
         }
         try
         {
         HorizontalAlignmentType align = para.getHorizontalAlignment();
         switch(align)
         {
            case RIGHT :
               paragraph.setAlignment(Alignment.RIGHT);
               break;
            case CENTER :
               paragraph.setAlignment(Alignment.CENTER);
               break;
            case JUSTIFY :
            case FILLED :
               paragraph.setAlignment(Alignment.JUSTIFIED);
               break;
            default :
               break;
         }
         }
         catch( java.lang.NullPointerException ex)
         {
            ex.printStackTrace();
         }
         
         ret.add(paragraph);
      }
      return ret;
   }

   @Override
   public String getTextContent()
   {
      String content = "";
      try
      {
         TextDocument doc = TextDocument.loadDocument(targetFile);
         Iterator<Paragraph> iter = doc.getParagraphIterator();
         while (iter.hasNext())
         {
            Paragraph para = iter.next();
            String text = para.getTextContent();
            content += text;
         }
         return content;
      }
      catch (Exception e)
      {
         new LntException("Error getting ODT file content : " + targetFile.getAbsolutePath(), e);
      }
      return "";
   }
   
   private Font getFont(Paragraph paragraph)
   {
      ParagraphStyleHandler handler = paragraph.getStyleHandler();
      ScriptType type = Document.ScriptType.WESTERN;
      
      // A font includes font family name, font style, font color, font size
      Font font = null;
      TextProperties textProperties = handler.getTextPropertiesForRead();
      if (textProperties != null) {
         font = textProperties.getFont(type);
      } else {
         font = new Font(null, null, 0, (StyleTypeDefinitions.TextLinePosition) null);
      }

      if (font != null && font.getFamilyName() != null && font.getColor() != null && font.getSize() != 0
            && font.getFontStyle() != null && font.getTextLinePosition() != null) {
         return font;
      }

      if (font.getColor() == null) {
         font.setColor(Color.BLACK);
      }
      if (font.getFontStyle() == null) {
         font.setFontStyle(FontStyle.REGULAR);
      }
      if (font.getTextLinePosition() == null) {
         font.setTextLinePosition(TextLinePosition.REGULAR);
      }
      return font;
   }
}
