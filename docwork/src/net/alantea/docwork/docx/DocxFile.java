package net.alantea.docwork.docx;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyle;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTStyles;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHexColor;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHighlightColor.Enum;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;

import net.alantea.docwork.DocWorkFile;
import net.alantea.docwork.DocWorkParagraph;
import net.alantea.docwork.DocWorkSpan;
import net.alantea.docwork.DocWorkParagraph.Alignment;
import net.alantea.utils.exception.LntException;

/**
 * The Class DocxFile.
 */
public class DocxFile extends DocWorkFile
{
   static final int INT_BLACK = 1;
   static final int INT_BLUE = 2;
   static final int INT_CYAN = 3;
   static final int INT_GREEN = 4;
   static final int INT_MAGENTA = 5;
   static final int INT_RED = 6;
   static final int INT_YELLOW = 7;
   static final int INT_WHITE = 8;
   static final int INT_DARK_BLUE = 9;
   static final int INT_DARK_CYAN = 10;
   static final int INT_DARK_GREEN = 11;
   static final int INT_DARK_MAGENTA = 12;
   static final int INT_DARK_RED = 13;
   static final int INT_DARK_YELLOW = 14;
   static final int INT_DARK_GRAY = 15;
   static final int INT_LIGHT_GRAY = 16;
   static final int INT_NONE = 17;
   
   /** The target file. */
   private File targetFile;
   
   /** The xdoc. */
   private XWPFDocument xdoc;
   
   /**
    * Instantiates a new docx file.
    *
    * @param path the path
    */
   public DocxFile(String path)
   {
      this(new File(path));
   }
   

   /**
    * Instantiates a new docx file.
    *
    * @param inputfile the inputfile
    */
   public DocxFile(File inputfile)
   {
      try
      {
         if (inputfile.exists())
         {
            xdoc = new XWPFDocument(OPCPackage.open(inputfile));
         }
         else
         {
            // Open the template containing the base title types
            InputStream streamTemplate = DocxFile.class.getResourceAsStream("/net/alantea/docwork/docx/template.docx");
            xdoc = new XWPFDocument(OPCPackage.open(streamTemplate));
         }
         targetFile = inputfile;
      }
      catch (InvalidFormatException | IOException e)
      {
         new LntException("Error creating DOCX file information class : " + inputfile.getAbsolutePath(), e);
      }
   }

   /**
    * Save.
    */
   @Override
   public void save()
   {
      try
      {
         if (targetFile.exists())
         {
            targetFile.delete();
         }
         xdoc.write(new FileOutputStream(targetFile));
      }
      catch (IOException e)
      {
         new LntException("Error saving DOCX file : " + targetFile.getAbsolutePath(), e);
      }
   }

   /**
    * Close.
    */
   @Override
   public void close()
   {
      try
      {
         xdoc.getPackage().close();
      }
      catch (IOException e)
      {
         new LntException("Error closing DOCX file : " + targetFile.getAbsolutePath(), e);
      }
   }

   /**
    * Append paragraph.
    *
    * @param text the text
    */
   @Override
   public void appendParagraph(String text)
   {
      XWPFParagraph paragraph = xdoc.createParagraph();
      XWPFRun run = paragraph.createRun();
      run.setText(text);
   }

   /**
    * Append paragraph.
    *
    * @param para the para
    */
   @Override
   public void appendParagraph(DocWorkParagraph para)
   {
      XWPFParagraph paragraph = xdoc.createParagraph();
      paragraph.setPageBreak(para.isPageBreak());
      if (para.getHeadingLevel() > 0)
      {
         String txt = "";
         for (DocWorkSpan docspan : para.getContent())
         {
            txt += docspan.getContent();
         }

         Pattern p = Pattern.compile("^.*w:val=\"heading " + para.getHeadingLevel() + "\".*$");
         try
         {
            CTStyles styles = xdoc.getStyle();
            for (CTStyle style : styles.getStyleList())
            {
               Matcher m = p.matcher(style.getName().toString());
               boolean b = m.matches();
               if(b)
               {
                  paragraph.setStyle(style.getStyleId());
               }
            }
         }
         catch (XmlException | IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         XWPFRun run = paragraph.createRun();
         run.setText(txt);
      }
      else
      {
         exploreParagraphSpans(para, paragraph);
      }
   }
   
   /**
    * Explore paragraph spans.
    *
    * @param para the para
    * @param paragraph the paragraph
    */
   private void exploreParagraphSpans(DocWorkParagraph para, XWPFParagraph paragraph)
   {
      for (DocWorkSpan span : para.getContent())
      {
         XWPFRun run = paragraph.createRun();
         run.setText(span.getContent());
         run.setBold(span.isBold());
         run.setItalic(span.isItalic());
         run.setFontSize((int) span.getFontSize());
         run.setFontFamily(span.getFontFamily());
         Color color = span.getFontColor();
         if (color != null)
         {           
            run.setColor(String.format("%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue()));
         }
         Color backcolor = span.getBackgroundColor();
         if (backcolor != null)
         {           
            CTRPr cTRpr = run.getCTR().getRPr();
            if (cTRpr == null)
            {
               cTRpr = run.getCTR().addNewRPr();
            }
            CTShd cTShd = cTRpr.getShd();
            if (cTShd == null)
            {
               cTShd = cTRpr.addNewShd();
            }
            cTShd.setVal(STShd.CLEAR);
            cTShd.setColor("auto");
            cTShd.setFill(String.format("%02X%02X%02X", backcolor.getRed(), backcolor.getGreen(), backcolor.getBlue()));
         }
      }
      Alignment align = para.getAlignment();
      switch(align)
      {
         case RIGHT :
            paragraph.setAlignment(ParagraphAlignment.RIGHT);
            break;
         case CENTER :
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            break;
         case JUSTIFIED :
            paragraph.setAlignment(ParagraphAlignment.BOTH);
            break;
         default :
            paragraph.setAlignment(ParagraphAlignment.LEFT);
            break;
      }
   }
   
   /**
    * Calculate size.
    *
    * @return the int
    */
   @Override
   public int calculateSize()
   {
      int size = 0;
      for (XWPFParagraph paragraph : xdoc.getParagraphs())
      {
         for (XWPFRun run : paragraph.getRuns())
         {
            String text = run.getText(0);
            size += text.length();
         }
      }
      return size;
   }


   /**
    * Gets the paragraphs.
    *
    * @return the paragraphs
    */
   @Override
   public List<DocWorkParagraph> getParagraphs()
   {
      List<DocWorkParagraph> ret = new LinkedList<>();

      for (XWPFParagraph paragraph : xdoc.getParagraphs())
      {
         DocWorkParagraph para = new DocWorkParagraph();
         para.setPageBreak(paragraph.isPageBreak());
         
         exploreParagraphStyle(paragraph, para);
         
         XWPFRun run0 = null;
         for (XWPFRun run : paragraph.getRuns())
         {
            if (run0 == null)
            {
               para.setBold(run.isBold());
               para.setItalic(run.isItalic());
               para.setFontSize(run.getFontSize());
               para.setFontFamily(run.getFontFamily());
            }
            DocWorkSpan span = new DocWorkSpan(run.getText(0));
            span.setBold(run.isBold());
            span.setItalic(run.isItalic());
            span.setFontSize(run.getFontSize());
            span.setFontFamily(run.getFontFamily());
            String color = run.getColor();
            if (color != null)
            {
               span.setFontColor(Color.decode("0x" + color));
            }
            Enum backcolor = run.getTextHightlightColor();
            System.out.println(backcolor.intValue());
            if (backcolor.intValue() != INT_NONE)
            {
               // TODO choose color
               span.setBackgroundColor(Color.LIGHT_GRAY);
            }
            else
            {
               CTShd cTShd = run.getCTR().getRPr().getShd();
               if (cTShd != null)
               {
                  STHexColor hex = cTShd.xgetFill();
                  String str = hex.getStringValue();
                  System.out.println(str);
                  span.setBackgroundColor(Color.decode("0x" + color));
                  //.setFill(String.format("%02X%02X%02X", backcolor.getRed(), backcolor.getGreen(), backcolor.getBlue()));
               }
            }
            para.add(span);
         }
         
         ParagraphAlignment align = paragraph.getAlignment();
         switch(align)
         {
            case RIGHT :
               para.setAlignment(Alignment.RIGHT);
               break;
            case CENTER :
               para.setAlignment(Alignment.CENTER);
               break;
            case BOTH :
               para.setAlignment(Alignment.JUSTIFIED);
               break;
            default :
               para.setAlignment(Alignment.LEFT);
               break;
         }
         ret.add(para);
      }
      return ret;
   }

   @Override
   public String getTextContent()
   {
      String content = "";
      for (XWPFParagraph paragraph : xdoc.getParagraphs())
      {
         for (XWPFRun run : paragraph.getRuns())
         {
            String text = run.getText(0);
            content += text;
         }
         content += "\n";
      }
      return content;
   }

   private void exploreParagraphStyle(XWPFParagraph paragraph, DocWorkParagraph para)
   {
      String styleId = paragraph.getStyle();
      if (styleId != null)
      {
         try
         {
            CTStyles styles = xdoc.getStyle();
            for (CTStyle style : styles.getStyleList())
            {
               if (style.getStyleId().equals(styleId))
               {
                  Pattern p = Pattern.compile("^.*w:val=\"heading ([0-9])\".*$");
                  Matcher m = p.matcher(style.getName().toString());
                  boolean b = m.matches();
                  if(b)
                  {
                     int n = Integer.parseInt(m.group(1));
                     para.setHeadingLevel(n);
                  }
               }
            }
         }
         catch (XmlException | IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }
}
