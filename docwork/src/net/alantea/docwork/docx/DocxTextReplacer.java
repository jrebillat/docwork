package net.alantea.docwork.docx;

import java.util.LinkedList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.PositionInParagraph;
import org.apache.poi.xwpf.usermodel.TextSegment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * The Class DocxTextReplacer.
 */
public class DocxTextReplacer
{
   
   /**
    * Replace a text in a paragraph.
    *
    * @param paragraph the paragraph
    * @param original the original text to replace
    * @param newText the new text to inject
    */
   public static void replace(XWPFParagraph paragraph, String original, String newText)
   {
      String paragraphText = paragraph.getText();
      int start = paragraphText.indexOf(original);
      replace(paragraph, start, start + original.length(), newText);
   }
   
   /**
    * Replace a text in a paragraph.
    *
    * @param paragraph the paragraph
    * @param start the start position to replace
    * @param end the end position to replace
    * @param newText the new text to inject
    */
   public static void replace(XWPFParagraph paragraph, int start, int end, String newText)
   {
      List<XWPFRun> runs = paragraph.getRuns();
      if (runs.size() == 1)
      {
         replacePartOfRun(runs.get(0), start, end, newText);
      }
      else if (!runs.isEmpty())
      {
         String paragraphText = paragraph.getText();
         replaceInRuns(paragraph, paragraphText.substring(start, end), newText);
      }
   }

   /**
    * Replace part of run.
    *
    * @param run the run
    * @param original the original
    * @param newText the new text
    */
   private static void replacePartOfRun(XWPFRun run, String original, String newText)
   {
      String runText = run.getText(0);
      if (runText.contains(original))
      {
         String startText = runText.substring(0, runText.indexOf(original));
         String endText = runText.substring(startText.length() + original.length());
         run.setText(startText + newText + endText, 0);
      }
   }

   /**
    * Replace part of run.
    *
    * @param run the run
    * @param start the start
    * @param end the end
    * @param newText the new text
    */
   private static void replacePartOfRun(XWPFRun run, int start, int end, String newText)
   {
      String runText = run.getText(0);
      String startText = runText.substring(0, start);
      String endText = runText.substring(end);
      
      run.setText(startText + newText + endText, 0);
   }

   /**
    * Replace in runs.
    *
    * @param paragraph the paragraph
    * @param originalText the original text
    * @param newText the new text
    */
   private static void replaceInRuns(XWPFParagraph paragraph, String originalText, String newText)
   {
      TextSegment segment = paragraph.searchText(originalText, new PositionInParagraph(0, 0, 0));
      int beginRun = segment.getBeginRun();
      int endRun = segment.getEndRun();
      
      if (beginRun == endRun)
      {
         replacePartOfRun(paragraph.getRuns().get(beginRun), originalText, newText);
      }
      else
      {
         replacePartOfRuns(paragraph, beginRun, endRun, originalText, newText);
      }
   }
   
   /**
    * Replace part of runs.
    *
    * @param paragraph the paragraph
    * @param beginRun the begin run
    * @param endRun the end run
    * @param originalText the original text
    * @param newText the new text
    */
   private static void replacePartOfRuns(XWPFParagraph paragraph, int beginRun, int endRun, String originalText, String newText)
   {
      List<XWPFRun> runs = paragraph.getRuns();
      int correspondingSize = countMatchingEndLength(runs.get(beginRun), originalText, true);
      if (correspondingSize == Integer.MAX_VALUE)
      {
         replacePartOfRun(runs.get(beginRun), originalText, newText);
      }
      else
      {
         List<Integer> runsToDelete = new LinkedList<>();

         // Manage first run with part of text
         String runText = runs.get(beginRun).getText(0);
         String toBeReplacedText = runText.substring(runText.length() - correspondingSize);
         replacePartOfRun(runs.get(beginRun), toBeReplacedText, newText);
         
         // Manage intermediate runs
         int currentRun = beginRun + 1;
         String buffer = originalText.substring(toBeReplacedText.length());
         correspondingSize = countMatchingEndLength(runs.get(currentRun), originalText.substring(toBeReplacedText.length()), false);
         while ((correspondingSize == Integer.MAX_VALUE) && (currentRun < runs.size()))
         {
            if (!runsToDelete.contains(currentRun))
            {
               runsToDelete.add(currentRun);
            }
            buffer = buffer.substring(runs.get(currentRun).getText(0).length());
            currentRun++;
            if (currentRun < runs.size())
            {
               runText = runs.get(currentRun).getText(0);
               correspondingSize = countMatchingEndLength(runs.get(currentRun), buffer, false);
               if ((correspondingSize == runText.length()) || (correspondingSize < 0) || (correspondingSize == Integer.MAX_VALUE))
               {
                  if (!runsToDelete.contains(currentRun))
                  {
                     runsToDelete.add(currentRun);
                  }
               }
            }
         }
         
         // Manage last run with part of text
         if ((currentRun < runs.size()) && (!runsToDelete.contains(currentRun)))
         {
            replacePartOfRun(runs.get(currentRun), 0, (correspondingSize < 0) ? (-correspondingSize) : correspondingSize, " ");
         }
         int delta = 0;
         for (Integer num : runsToDelete)
         {
            boolean done = paragraph.removeRun(num - delta);
            delta++;
            if (!done)
            {
               runs.get(num - delta).setText(" ");
            }
         }
      }
   }

   /**
    * Count matching end length.
    *
    * @param run the run
    * @param toFind the to find
    * @param starting the starting
    * @return the int
    */
   @SuppressWarnings("unused")
   private static int countMatchingEndLength(XWPFRun run, String toFind, boolean starting)
   {
      String runText = run.getText(0);
      if (runText.startsWith(toFind))
      {
         return - toFind.length();
      }
      if (!starting && (runText.length() < toFind.length()) && (toFind.startsWith(runText)))
      {
         return Integer.MAX_VALUE;
      }
      
      for (int i = 0; i < runText.length() -1; i++)
      {
         String toSearch = toFind.substring(0, runText.length() - i);
         if (runText.substring(i).equals(toSearch));
         {
            return runText.length() - i;
         }
      }
      return Integer.MAX_VALUE;
   }
}
