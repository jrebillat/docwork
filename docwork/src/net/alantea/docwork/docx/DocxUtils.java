package net.alantea.docwork.docx;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DocxUtils.
 */
public class DocxUtils
{
   
   /** The Constant STYLE_HEADING1. */
   public static final String STYLE_HEADING1 = "Titre1";
   
   /** The Constant STYLE_HEADING2. */
   public static final String STYLE_HEADING2 = "Titre2";
   
   /** The Constant STYLE_HEADING3. */
   public static final String STYLE_HEADING3 = "Titre3";
   
   /** The Constant STYLE_HEADING4. */
   public static final String STYLE_HEADING4 = "Titre4";
   
   /** The Constant STYLE_NORMAL. */
   public static final String STYLE_NORMAL = "Normal";
   
   /** The Constant INSERT_BEFORE. */
   public static final int INSERT_BEFORE = 1;
   
   /** The Constant INSERT_REPLACE. */
   public static final int INSERT_REPLACE = 2;
   
   /** The Constant INSERT_AFTER. */
   public static final int INSERT_AFTER = 3;

   /** The logger. */
   private static Logger logger = LoggerFactory.getLogger(DocxUtils.class);

   /**
    * Open file.
    *
    * @param filePath the file path
    * @return the XWPF document
    */
   public static XWPFDocument openFile(String filePath)
   {
      try
      {
         return new XWPFDocument(OPCPackage.open(filePath));
      }
      catch (InvalidFormatException | IOException e)
      {
         e.printStackTrace();
         logger.error("Unable to load document '" + filePath + "'", e);
         return null;
      }
   }
   
   /**
    * Save in file and close document.
    *
    * @param document the document
    * @param outputPath the output path
    * @return true, if successful
    */
   public static boolean saveInFile(XWPFDocument document, String outputPath)
   {
      try
      {
         document.write(new FileOutputStream(outputPath));
         document.getPackage().close();
      }
      catch (IOException e)
      {
         e.printStackTrace();
         logger.error("Unable to load document '" + outputPath + "'", e);
         return false;
      }
      return true;
   }
   
   /**
    * Execute on all paragraphs.
    *
    * @param document the document
    * @param executor the executor
    */
   public static void executeOnAllParagraphs(XWPFDocument document, ParagraphExecutor executor)
   {
      internalExecuteOnAllParagraphs(document.getParagraphs(), executor);

      for (XWPFTable tbl : document.getTables())
      {
         for (XWPFTableRow row : tbl.getRows())
         {
            for (XWPFTableCell cell : row.getTableCells())
            {
               internalExecuteOnAllParagraphs(cell.getParagraphs(), executor);
            }
         }
      }
   }
   
   /**
    * Internal execute on all paragraphs.
    *
    * @param paragraphs the paragraphs
    * @param executor the executor
    */
   public static void internalExecuteOnAllParagraphs(List<XWPFParagraph> paragraphs, ParagraphExecutor executor)
   {
      List<XWPFParagraph> list = new LinkedList<>(paragraphs);
      for (XWPFParagraph paragraph : list)
      {
         executor.execute(paragraph);
      }
   }
   
   /**
    * Change all text from a paragraph.
    *
    * @param paragraph the paragraph
    * @param newText the new text
    */
   public static void changeParagraphContent(XWPFParagraph paragraph, String newText)
   {
      changeParagraphContent(paragraph, null, newText);
   }
   
   /**
    * Change all text from a paragraph, with a style.
    *
    * @param paragraph the paragraph
    * @param styleName the style name
    * @param newText the new text
    */
   public static void changeParagraphContent(XWPFParagraph paragraph, String styleName, String newText)
   {
      if (styleName != null)
      {
         paragraph.setStyle(styleName);
      }
      List<XWPFRun> runs = paragraph.getRuns();
      if (runs.isEmpty())
      {
         paragraph.createRun();
      }
      runs = paragraph.getRuns();
      for (int i = runs.size() - 1; i > 0; i--)
      {
         paragraph.removeRun(i);
      }
      XWPFRun run = runs.get(0);
      run.setText(newText, 0);
   }
   
   /**
    * Insert text before given paragraph.
    *
    * @param paragraph the paragraph
    * @param styleName the style name
    * @param newText the new text
    */
   public static void insertNewParagraphBefore(XWPFParagraph paragraph, String styleName, String newText)
   {
      XmlCursor cur = paragraph.getCTP().newCursor();
      XWPFParagraph newParagraph = paragraph.getDocument().insertNewParagraph(cur);
      changeParagraphContent(newParagraph, styleName, newText);
   }
   
   /**
    * Insert text after given paragraph.
    *
    * @param paragraph the paragraph
    * @param styleName the style name
    * @param newText the new text
    */
   public static void insertNewParagraphAfter(XWPFParagraph paragraph, String styleName, String newText)
   {
      XmlCursor cur = paragraph.getCTP().newCursor();
      cur.toNextSibling();
      XWPFParagraph newParagraph = paragraph.getDocument().insertNewParagraph(cur);
      changeParagraphContent(newParagraph, styleName, newText);
   }

   /**
    * Replace given paragraph with a table.
    *
    * @param paragraph the paragraph
    * @param contents the contents
    * @param howToInsert the how to insert
    */
   public static void replaceParagraphWithTable(XWPFParagraph paragraph, Object[][] contents, int howToInsert)
   {
      replaceParagraphWithTable(paragraph, contents, null, false, howToInsert);
   }

   /**
    * Replace given paragraph with a table and an header line.
    *
    * @param paragraph the paragraph
    * @param contents the contents
    * @param firstRowIsHeader the first row is header
    * @param howToInsert the how to insert
    */
   public static void replaceParagraphWithTable(XWPFParagraph paragraph, Object[][] contents, boolean firstRowIsHeader,
         int howToInsert)
   {
      replaceParagraphWithTable(paragraph, contents, null, firstRowIsHeader, howToInsert);
   }

   /**
    * Replace given paragraph with a table given the percentages for each column.
    *
    * @param paragraph the paragraph
    * @param contents the contents
    * @param percentages the percentages
    * @param howToInsert the how to insert
    */
   public static void replaceParagraphWithTable(XWPFParagraph paragraph, Object[][] contents, int[] percentages,
         int howToInsert)
   {
      replaceParagraphWithTable(paragraph, contents, percentages, false, howToInsert);
   }

   /**
    * Replace given paragraph with a table and an header line given the percentages for each column.
    *
    * @param paragraph the paragraph
    * @param contents the contents
    * @param percentages the percentages
    * @param firstRowIsHeader the first row is header
    * @param howToInsert the how to insert
    */
   public static void replaceParagraphWithTable(XWPFParagraph paragraph, Object[][] contents, int[] percentages,
         boolean firstRowIsHeader, int howToInsert)
   {
      XmlCursor cursor = paragraph.getCTP().newCursor();
      if (howToInsert == INSERT_AFTER)
      {
         cursor.toNextSibling();
      }
      XWPFTable table = paragraph.getDocument().insertNewTbl(cursor);
      table.setWidth("100%");

      int nbRows = contents.length;
      int nbCols = contents[0].length;
      for (int i = 0; i < nbRows; i++)
      {
         XWPFTableRow row = table.createRow();
         for (int j = 0; j < nbCols; j++)
         {
            if (j > 0)
            {
               row.addNewTableCell();
            }
            int percentage = (int) (100.0 / nbCols);
            if (percentages != null)
            {
               percentage = percentages[j];
            }
            row.getCell(j).setWidth("" + percentage + "%");
            XWPFParagraph insideParagraph = row.getCell(j).getParagraphs().get(0);
            if ((i == 0) && firstRowIsHeader)
            {
               XWPFRun r1 = insideParagraph.createRun();
               r1.setBold(true);
               r1.setText(contents[i][j].toString());
               row.getCell(j).getCTTc().addNewTcPr().addNewShd().setFill("DDDDDD");
            }
            else
            {
               row.getCell(j).setText(contents[i][j].toString());
            }
            insideParagraph.setAlignment(ParagraphAlignment.CENTER);
            row.getCell(j).setVerticalAlignment(XWPFTableCell.XWPFVertAlign.CENTER);
         }
      }
      table.removeRow(0);
      if (howToInsert == INSERT_REPLACE)
      {
         changeParagraphContent(paragraph, "");
      }
   }

   /**
    * Replace a part of text in a paragraph.
    *
    * @param paragraph the paragraph
    * @param start the start position to replace
    * @param end the end position to replace
    * @param replacingText the replacing text
    */
   public static void replaceTextInParagraph(XWPFParagraph paragraph, int start, int end, String replacingText)
   {
      DocxTextReplacer.replace(paragraph, start, end, replacingText);
   }

   /**
    * Replace a text in a paragraph.
    *
    * @param paragraph the paragraph
    * @param textToBeReplaced the text to be replaced
    * @param replacingText the replacing text
    */
   public static void replaceTextInParagraph(XWPFParagraph paragraph, String textToBeReplaced, String replacingText)
   {
      DocxTextReplacer.replace(paragraph, textToBeReplaced, replacingText);
   }
   
   @FunctionalInterface
   public interface ParagraphExecutor
   {
      public void execute(XWPFParagraph paragraph);
   }
}
