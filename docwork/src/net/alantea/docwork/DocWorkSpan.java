package net.alantea.docwork;

/**
 * The Class DocWorkSpan.
 */
public class DocWorkSpan extends DocWorkBaseElement
{
   
   /** The content. */
   private String content;

   /**
    * Instantiates a new doc work span.
    *
    */
   public DocWorkSpan()
   {
      this("");
   }

   /**
    * Instantiates a new doc work span.
    *
    * @param content the content
    */
   public DocWorkSpan(String content)
   {
      this.content = content;
   }

   /**
    * Gets the content.
    *
    * @return the content
    */
   public String getContent()
   {
      return content;
   }

   /**
    * Sets the content.
    *
    * @param content the new content
    */
   public void setContent(String content)
   {
      this.content = content;
   }

   /**
    * Gets the text.
    *
    * @return the text
    */
   public String getText()
   {
     return getContent();
   }
}
