package net.alantea.docwork.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import net.alantea.tools.zip.ZipInputStreamContainer;

public final class Utils
{
   private static final Pattern TAG_OO_REGEX = Pattern.compile("<text:p.*?>(.*?)</text:p>", Pattern.DOTALL);
   private static final Pattern TAG_WORD_REGEX = Pattern.compile("<w:t.*?>(.*?)</w:t>", Pattern.DOTALL);
   
   private static int count;

   private Utils() {}

   /**
    * Gets the characters count.
    *
    * @param path the path
    * @return the count
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static int getCount(String path) throws IOException
   {
      if (path.endsWith(".docx"))
      {
         return getWordCount(path);
      }
      else if (path.endsWith(".odt"))
      {
         return getOOCount(path);
      }
      else
      {
         return 0;
      }
   }

   /**
    * Gets the text.
    *
    * @param path the path
    * @return the text
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static List<String> getText(String path) throws IOException
   {
      if (path.endsWith(".docx"))
      {
         return getWordText(path);
      }
      else if (path.endsWith(".odt"))
      {
         return getOOText(path);
      }
      else
      {
         return new LinkedList<>();
      }
   }
   
   /**
    * Gets the complete text.
    *
    * @param path the path
    * @return the complete text
    * @throws IOException Signals that an I/O exception has occurred.
    */
   public static String getCompleteText(String path) throws IOException
   {
      String ret = "";
      for (String paragraph : getText(path))
      {
         ret += paragraph + "\n";
      }
      return ret;
   }

   /**
    * Gets the OO count.
    *
    * @param path the path
    * @return the OO count
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private static int getOOCount(String path) throws IOException
   {
      count = 0;
      ZipInputStreamContainer cont = new ZipInputStreamContainer(path);

      InputStream istream = cont.getFile("content.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

      stream.forEach(Utils::getOOText);

      bufreader.close();
      return count;
   }
   
   /**
    * Gets the OO text.
    *
    * @param line the line
    * @return the OO text
    */
   private static List<String> getOOText(String line)
   {
      line = line.replaceAll("&\\S+;", ".");
      Matcher matcher = TAG_OO_REGEX.matcher(line);
      while (matcher.find())
      {
         String found = matcher.group(1);
         found = found.replaceAll("<text:span.*?>", "");
         found = found.replaceAll("</text:span>", "");
         found = found.replaceAll("<text:s/>", " ");
         found = found.replaceAll("<text:soft-page-break/>", "");
         found = found.replaceAll("<text:bookmark-start.*?/>", "");
         found = found.replaceAll("<text:bookmark-end.*?/>", "");
         found = found.replaceAll("<text:p.*?>", "");
         found = found.replaceAll("\\?\\?", " ?");
         found = found.replaceAll("\\?!", " !");
         count += found.length();
      }
      return null;
   }

   /**
    * Gets the word count.
    *
    * @param path the path
    * @return the word count
    * @throws IOException Signals that an I/O exception has occurred.
    */
   private static int getWordCount(String path) throws IOException
   {
      count = 0;
      ZipInputStreamContainer cont = new ZipInputStreamContainer(path);
      
      InputStream istream = cont.getFile("word/document.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

      stream.forEach(Utils::getWordText);
         
     bufreader.close();
     return count;
   }
   
   /**
    * Gets the word text.
    *
    * @param line the line
    * @return the word text
    */
   private static List<String> getWordText(String line)
   {
      Matcher matcher = TAG_WORD_REGEX.matcher(line);
      while (matcher.find())
      {
         count += matcher.group(1).length();
      }
      return null;
   }
}
