package net.alantea.tablerwork;

import java.io.File;
import java.io.IOException;
import java.util.List;

import net.alantea.tablerwork.xlsx.XlsxFile;


/**
 * The Class TablerWorkFile.
 */
public abstract class TablerWorkFile
{
   
   /**
    * The Enum Alignment.
    */
   public enum Alignment {
      
      /** The default. */
      DEFAULT,
      
      /** The center. */
      CENTER,
      
      /** The verticalcenter. */
      VERTICALCENTER,
      
      /** The horizontalcenter. */
      HORIZONTALCENTER,
      
      /** The top. */
      TOP,
      
      /** The bottom. */
      BOTTOM,
      
      /** The left. */
      LEFT,
      
      /** The right. */
      RIGHT,
      
      /** The topleft. */
      TOPLEFT,
      
      /** The topright. */
      TOPRIGHT,
      
      /** The bottomleft. */
      BOTTOMLEFT,
      
      /** The bottomright. */
      BOTTOMRIGHT;
   }

   /**
    * The Enum Format.
    */
   public enum Format {
      
      /** The general. */
      GENERAL(0),
      
      /** The text. */
      TEXT(0),
      
      /** The integer. */
      INTEGER(1),
      
      /** The double. */
      DOUBLE(2),
      
      /** The percentage. */
      PERCENTAGE(9),
      
      /** The percentage2. */
      PERCENTAGE2(0xa),
      
      /** The date. */
      DATE(0xe),
      
      /** The time. */
      TIME(0x14),
      
      /** The timeseconds. */
      TIMESECONDS(0x15),
      
      /** The datetime. */
      DATETIME(0x16);

      /** The value. */
      public final int value;

      /**
       * Instantiates a new format.
       *
       * @param value the value
       */
      private Format(int value)
      {
          this.value = value;
      }
   }
   
   /**
    * Create tabler work file.
    *
    * @param prefix the prefix
    * @param extension the extension
    * @return the tabler work file
    */
   public static final TablerWorkFile createTemporaryTablerWorkFile(String prefix, String extension)
   {
      File file;
      try
      {
         file = File.createTempFile(prefix, extension);
         file.deleteOnExit();
         return loadTablerWorkFile(file);
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      return null;
   }
   
   /**
    * Load tabler work file.
    *
    * @param path the path
    * @return the tabler work file
    */
   public static final TablerWorkFile loadTablerWorkFile(String path)
   {
      return loadTablerWorkFile(new File(path));
   }
   

   /**
    * Load tabler work file.
    *
    * @param inputfile the input file
    * @return the tabler work file
    */
   public static final TablerWorkFile loadTablerWorkFile(File inputfile)
   {
      if (inputfile.getName().endsWith(".xlsx"))
      {
         return new XlsxFile(inputfile);
      }
//      else if (inputfile.getName().endsWith(".ods"))
//      {
//         return new OdsFile(inputfile);
//      }
      return null;
   }
   
   /**
    * Gets the file path.
    *
    * @return the file path
    */
   public abstract String getFilePath();
   
   /**
    * Save.
    */
   public abstract void save();
   
   /**
    * Save as.
    *
    * @param newTargetFile the new target file
    */
   public abstract void saveAs(File newTargetFile);
   

   /**
    * Gets the cell reference.
    *
    * @param rownum the rownum
    * @param columnref the columnref
    * @return the cell reference
    */
   public String getCellReference(int rownum, String columnref)
   {
      if ((!columnref.isEmpty()) && ((columnref.length() == 1) || (columnref.length() == 2)))
      {
          return columnref + rownum;
      }
      return null;
   }
   
   /**
    * Gets the sheet.
    *
    * @param sheetName the sheet name
    * @return the sheet
    */
   public abstract TablerWorkSheet getSheet(String sheetName);
   
   /**
    * Gets the sheet.
    *
    * @param sheetNumber the sheet number
    * @return the sheet
    */
   public abstract TablerWorkSheet getSheet(int sheetNumber);
   
   /**
    * Gets the number of sheets.
    *
    * @return the number of sheets
    */
   public abstract int getNumberOfSheets();
   
   /**
    * Creates the sheet.
    *
    * @param sheetName the sheet name
    * @return the tabler work sheet
    */
   public abstract TablerWorkSheet createSheet(String sheetName);
   
   /**
    * Gets the sheets.
    *
    * @return the sheets
    */
   public abstract List<TablerWorkSheet> getSheets();
   
   /**
    * Close file.
    */
   public abstract void close();

   /**
    * Execute on all sheets.
    *
    * @param executor the executor
    */
   public void executeOnAllSheets(TablerWorkCellExecutor executor)
   {
      for (int i = 0; i < getNumberOfSheets(); i++)
      {
         TablerWorkSheet sheet = getSheet(i);
         if (sheet != null)
         {
            sheet.executeOnAllRows(executor);
         }
      }
   }
}
