package net.alantea.tablerwork;

/**
 * The Class TablerWorkSheet.
 */
public abstract class TablerWorkSheet
{

   /**
    * Instantiates a new tabler work sheet.
    */
   public  TablerWorkSheet()
   {
   }

   /**
    * Gets the row.
    *
    * @param rowNumber the row number
    * @return the row
    */
   public abstract TablerWorkRow getRow(int rowNumber);

   /**
    * Gets the last row num.
    *
    * @return the last row num
    */
   public abstract int getLastRowNum();
   
   /**
    * Gets the first row num.
    *
    * @return the first row num
    */
   public abstract int getFirstRowNum();

   /**
    * Gets the sheet name.
    *
    * @return the sheet name
    */
   public abstract String getSheetName();
   

   /**
    * Merge cells.
    *
    * @param firstRow the first row
    * @param firstCol the first col
    * @param lastRow the last row
    * @param lastCol the last col
    */
   public abstract void mergeCells(int firstRow, int firstCol, int lastRow, int lastCol);

   /**
    * Adds the cell borders.
    *
    * @param startrownum the startrownum
    * @param startcellnum the startcellnum
    * @param nbrows the nbrows
    * @param nbcells the nbcells
    */
   public abstract void addCellBorders(int startrownum, int startcellnum, int nbrows, int nbcells);
   
   /**
    * Execute on all rows.
    *
    * @param executor the executor
    */
   public void executeOnAllRows(TablerWorkCellExecutor executor)
   {
      for (int j = getFirstRowNum(); j <= getLastRowNum(); j++)
      {
         TablerWorkRow row = getRow(j);
         if (row != null)
         {
            row.executeOnAllCells(executor);
         }
      }
   }
}
