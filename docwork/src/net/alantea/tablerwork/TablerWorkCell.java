package net.alantea.tablerwork;

import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class TablerWorkCell.
 */
public abstract class TablerWorkCell
{
   
   /** The Constant cellrefPattern. */
   private final static Pattern cellrefPattern = Pattern.compile("^\\s*([A-Z]+)\\s*$");
   
   /** The row. */
   private TablerWorkRow row;
   
   /** The cell number. */
   private int cellNumber;

   /**
    * Instantiates a new tabler work cell.
    *
    * @param row the row
    * @param num the num
    */
   public TablerWorkCell(TablerWorkRow row, int num)
   {
      this.row = row;
      this.cellNumber = num;
   }

   /**
    * Instantiates a new tabler work cell.
    *
    * @param row the row
    * @param reference the reference
    */
   public TablerWorkCell(TablerWorkRow row, String reference)
   {
      this(row, getCellReferenceAsInt(reference));
   }
   
   /**
    * Gets the integer value.
    *
    * @return the integer value
    */
   public abstract int getIntegerValue();
   
   /**
    * Gets the numeric value.
    *
    * @return the numeric value
    */
   public abstract double getNumericValue();
   
   /**
    * Gets the string value.
    *
    * @return the string value
    */
   public abstract String getStringValue();
   
   /**
    * Gets the hyperlink.
    *
    * @return the hyperlink
    */
   public abstract String getHyperlink();
   
   /**
    * Sets the value.
    *
    * @param value the new value
    */
   public abstract void setValue(String value);
   
   /**
    * Sets the value.
    *
    * @param value the new value
    */
   public abstract void setValue(int value);
   
   /**
    * Sets the value.
    *
    * @param value the new value
    */
   public abstract void setValue(double value);
   
   /**
    * Sets the hyperlink.
    *
    * @param link the new hyperlink
    */
   public abstract void setHyperlink(String link);

   /**
    * Gets the cell reference as integer.
    *
    * @param cellnum the cellnum
    * @return the cell reference as integer
    */
   public static String getCellReference(int cellnum)
   {
      String ret = "1";
      
      int ret0 = cellnum % 26;
      int ret1 = (cellnum - ret0)/26;
      if (ret1 > 0)
      {
         ret = intToAsciiString(ret1) + intToAsciiString(ret0);
      }
      else
      {
         ret = intToAsciiString(ret0);
      }
      return ret;
   }
   
   /**
    * Int to ascii string.
    *
    * @param value the value
    * @return the string
    */
   private static String intToAsciiString(int value)
   {
      int asciiRef = value + 64;
      return Character.toString((char) asciiRef);
   }

   /**
    * Gets the cell reference as integer.
    *
    * @param cellref the cellref
    * @return the cell reference as integer
    */
   public static int getCellReferenceAsInt(String cellref)
   {
      int ret = 0;

      Matcher m = cellrefPattern.matcher(cellref);
      if (m.find())
      {
         String ret0String = m.group(1);
         ret = 0;
         byte[] b = ret0String.getBytes(StandardCharsets.US_ASCII);
         byte[] a = "A".getBytes(StandardCharsets.US_ASCII);
         if (b.length == 1)
         {
            ret = (int)b[0] - (int)a[0] + 1;
         }
         else
         {
            ret = ((int)b[1] - (int)a[0] + 1) + ((int)b[0] - (int)a[0] + 1) * 26;
         }
      }
      return ret;
   }

   /**
    * Gets the row.
    *
    * @return the row
    */
   public TablerWorkRow getRow()
   {
      return row;
   }

   /**
    * Gets the cell number.
    *
    * @return the cell number
    */
   public int getCellNumber()
   {
      return cellNumber;
   }
   
   
}
