package net.alantea.tablerwork;

/**
 * The Class TablerWorkRow.
 */
public abstract class TablerWorkRow
{
   /** The sheet. */
   TablerWorkSheet sheet;
   
   /** The row number. */
   private int rowNumber;
   
   /**
    * Instantiates a new tabler work row.
    *
    * @param sheet the sheet
    * @param row the row
    */
   protected TablerWorkRow(TablerWorkSheet sheet, int row)
   {
      this.sheet = sheet;
      this.rowNumber = row;
   }

   /**
    * Gets the sheet.
    *
    * @return the sheet
    */
   public TablerWorkSheet getSheet()
   {
      return sheet;
   }
   
   /**
    * Gets the cell.
    *
    * @param cellReference the cell reference
    * @return the cell
    */
   public abstract TablerWorkCell getCell(String cellReference);
   
   /**
    * Gets the cell.
    *
    * @param cellNumber the cell number
    * @return the cell
    */
   public TablerWorkCell getCell(int cellNumber)
   {
      return getCell(TablerWorkCell.getCellReference(cellNumber));
   }
   
   /**
    * Gets the first cell num.
    *
    * @return the first cell num
    */
   public abstract int getFirstCellNum();
   
   /**
    * Gets the last cell num.
    *
    * @return the last cell num
    */
   public abstract int getLastCellNum();
   
   /**
    * Gets the cell reference.
    *
    * @param columnref the columnref
    * @return the cell reference
    */
   public String getCellReference(String columnref)
   {
      return columnref + rowNumber;
   }
   
   /**
    * Gets the integer value.
    *
    * @param cellReference the cell reference
    * @return the integer value
    */
   public abstract int getIntegerValue(String cellReference);
   
   /**
    * Gets the numeric value.
    *
    * @param cellReference the cell reference
    * @return the numeric value
    */
   public abstract double getNumericValue(String cellReference);
   
   /**
    * Gets the string value.
    *
    * @param cellReference the cell reference
    * @return the string value
    */
   public abstract String getStringValue(String cellReference);
   
   /**
    * Gets the hyperlink.
    *
    * @param cellReference the cell reference
    * @return the hyperlink
    */
   public abstract String getHyperlink(String cellReference);
   
   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   public abstract void setValue(String cellReference, String value);
   
   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   public abstract void setValue(String cellReference, int value);
   
   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   public abstract void setValue(String cellReference, double value);
   
   /**
    * Sets the hyperlink.
    *
    * @param cellReference the cell reference
    * @param link the link
    */
   public abstract void setHyperlink(String cellReference, String link);
   
   /**
    * Gets the integer value.
    *
    * @param cellNumber the cell number
    * @return the integer value
    */
   public int getIntegerValue(int cellNumber)
   {
      return getIntegerValue(TablerWorkCell.getCellReference(cellNumber));
   }
   
   /**
    * Gets the numeric value.
    *
    * @param cellNumber the cell number
    * @return the numeric value
    */
   public double getNumericValue(int cellNumber)
   {
      return getNumericValue(TablerWorkCell.getCellReference(cellNumber));
   }
   
   /**
    * Gets the string value.
    *
    * @param cellNumber the cell number
    * @return the string value
    */
   public String getStringValue(int cellNumber)
   {
      return getStringValue(TablerWorkCell.getCellReference(cellNumber));
   }
   
   /**
    * Gets the hyperlink.
    *
    * @param cellNumber the cell number
    * @return the hyperlink
    */
   public String getHyperlink(int cellNumber)
   {
      return getHyperlink(TablerWorkCell.getCellReference(cellNumber));
   }
   
   /**
    * Sets the value.
    *
    * @param cellNumber the cell number
    * @param value the value
    */
   public void setValue(int cellNumber, String value)
   {
      setValue(TablerWorkCell.getCellReference(cellNumber), value);
   }
   
   /**
    * Sets the value.
    *
    * @param cellNumber the cell number
    * @param value the value
    */
   public void setValue(int cellNumber, int value)
   {
      setValue(TablerWorkCell.getCellReference(cellNumber), value);
   }
   
   /**
    * Sets the value.
    *
    * @param cellNumber the cell number
    * @param value the value
    */
   public void setValue(int cellNumber, double value)
   {
      setValue(TablerWorkCell.getCellReference(cellNumber), value);
   }
   
   /**
    * Sets the hyperlink.
    *
    * @param cellNumber the cell number
    * @param link the link
    */
   public void setHyperlink(int cellNumber, String link)
   {
      setHyperlink(TablerWorkCell.getCellReference(cellNumber), link);
   }

   /**
    * Execute on all cells.
    *
    * @param executor the executor
    */
   public void executeOnAllCells(TablerWorkCellExecutor executor)
   {
      for (int k = getFirstCellNum(); k <= getLastCellNum(); k++)
      {
         if (k >= 0)
         {
             TablerWorkCell cell = getCell(k);
             if (cell != null)
             {
                executor.execute(cell);
             }
          }
       }
   }
}
