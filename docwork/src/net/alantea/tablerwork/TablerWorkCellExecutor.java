package net.alantea.tablerwork;

/**
 * The Interface TableWorkCellExecutor.
 */
@FunctionalInterface
public interface TablerWorkCellExecutor
{
   /**
    * Execute.
    *
    * @param cell the cell
    */
   public void execute(TablerWorkCell cell);
}