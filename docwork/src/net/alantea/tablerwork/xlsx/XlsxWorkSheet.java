package net.alantea.tablerwork.xlsx;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import net.alantea.tablerwork.TablerWorkRow;
import net.alantea.tablerwork.TablerWorkSheet;
/**
 * The Class XlxsWorkSheet.
 */
public class XlsxWorkSheet extends TablerWorkSheet
{
   
   /** The file. */
   private XlsxFile file;
   
   /** The sheet. */
   private XSSFSheet sheet;

   /**
    * Instantiates a new xlxs work sheet.
    *
    * @param file the file
    * @param sheet the sheet
    */
   public XlsxWorkSheet(XlsxFile file, XSSFSheet sheet)
   {
      this.sheet = sheet;   
   }
   
   /**
    * Gets the row.
    *
    * @param rowNumber the row number
    * @return the row
    */
   @Override
   public TablerWorkRow getRow(int rowNumber)
   {
      return new XlsxWorkRow(this, rowNumber);
   }

   /**
    * Gets the XSSF sheet.
    *
    * @return the XSSF sheet
    */
   public XSSFSheet getXSSFSheet()
   {
      return sheet;
   }
   
   /**
    * Gets the file.
    *
    * @return the file
    */
   public XlsxFile getFile()
   {
      return file;
   }
   
   /**
    * Gets the last row num.
    *
    * @return the last row num
    */
   @Override
   public int getLastRowNum()
   {
      return sheet.getLastRowNum();
   }

   /**
    * Gets the first row num.
    *
    * @return the first row num
    */
   @Override
   public int getFirstRowNum()
   {
      return sheet.getFirstRowNum();
   }

   /**
    * Gets the sheet name.
    *
    * @return the sheet name
    */
   @Override
   public String getSheetName()
   {
      return sheet.getSheetName();
   }

   @Override
   public void mergeCells(int firstRow, int firstCol, int lastRow, int lastCol)
   {
      boolean ok = firstRow >= 0;
      ok &= lastRow >= firstRow;
      ok &= firstCol >= 0;
      ok &= lastCol >= firstCol;
      ok &= (lastCol > firstCol) ||(lastRow > firstRow);
      if (ok)
      {
         sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
      }
   }

   @Override
   public void addCellBorders(int startrownum, int startcellnum, int nbrows, int nbcells)
   {
      for (int i = 0; i < nbrows; i++)
      {
         for (int j = 0; j < nbcells; j++)
         {
            CellRangeAddress region = new CellRangeAddress(startrownum + i, startrownum + i, startcellnum + j, startcellnum + j);
            RegionUtil.setBorderTop(BorderStyle.MEDIUM, region, sheet);
            RegionUtil.setBorderBottom(BorderStyle.MEDIUM, region, sheet);
            RegionUtil.setBorderLeft(BorderStyle.MEDIUM, region, sheet);
            RegionUtil.setBorderRight(BorderStyle.MEDIUM, region, sheet);
         }
      }
   }

}
