package net.alantea.tablerwork.xlsx;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import net.alantea.tablerwork.TablerWorkFile;
import net.alantea.tablerwork.TablerWorkSheet;
import net.alantea.utils.exception.LntException;

/**
 * The Class XlsxFile.
 */
public class XlsxFile extends TablerWorkFile
{
   /** The target file. */
   private File targetFile;
   
   /** The document. */
   private XSSFWorkbook xdoc;
   
   /**
    * Instantiates a new file.
    *
    * @param path the path
    */
   public XlsxFile(String path)
   {
      this(new File(path));
   }
   

   /**
    * Instantiates a new file.
    *
    * @param inputfile the inputfile
    */
   public XlsxFile(File inputfile)
   {
      try
      {
         if ((inputfile.exists()) && (inputfile.length() > 0))
         {
            xdoc = new XSSFWorkbook(OPCPackage.open(inputfile));
         }
         else
         {
            // Open the template containing the base title types
            InputStream streamTemplate = XlsxFile.class.getResourceAsStream("/net/alantea/tablerwork/xlsx/template.xlsx");
            xdoc = new XSSFWorkbook(OPCPackage.open(streamTemplate));
         }
         targetFile = inputfile;
      }
      catch (InvalidFormatException | IOException e)
      {
         new LntException("Error creating XLSX file information class : " + inputfile.getAbsolutePath(), e);
      }
   }
   
   /**
    * Gets the file path.
    *
    * @return the file path
    */
   public String getFilePath()
   {
      return targetFile.getAbsolutePath();
   }

   /**
    * Save.
    */
   @Override
   public void save()
   {
      try
      {
         if (targetFile.exists())
         {
            targetFile.delete();
         }
         xdoc.write(new FileOutputStream(targetFile));
      }
      catch (IOException e)
      {
         new LntException("Error saving XLSX file : " + targetFile.getAbsolutePath(), e);
      }
   }
   
   @Override
   public void saveAs(File newTargetFile)
   {
      targetFile = newTargetFile;
      save();
   }


   /**
    * Gets the sheet.
    *
    * @param sheetName the sheet name
    * @return the sheet
    */
   @Override
   public TablerWorkSheet getSheet(String sheetName)
   {
      // TODO Auto-generated method stub
      return new XlsxWorkSheet(this, xdoc.getSheet(sheetName));
   }
   
   /**
    * Gets the sheet.
    *
    * @param sheetNumber the sheet number
    * @return the sheet
    */
   @Override
   public TablerWorkSheet getSheet(int sheetNumber)
   {
      // TODO Auto-generated method stub
      return new XlsxWorkSheet(this, xdoc.getSheetAt(sheetNumber));
   }


   /**
    * Gets the number of sheets.
    *
    * @return the number of sheets
    */
   @Override
   public int getNumberOfSheets()
   {
      // TODO Auto-generated method stub
      return 0;
   }


   /**
    * Creates the sheet.
    *
    * @param sheetName the sheet name
    * @return the tabler work sheet
    */
   @Override
   public TablerWorkSheet createSheet(String sheetName)
   {
      // TODO Auto-generated method stub
      return new XlsxWorkSheet(this, xdoc.createSheet(sheetName));
   }


   /**
    * Gets the sheets.
    *
    * @return the sheets
    */
   @Override
   public List<TablerWorkSheet> getSheets()
   {
      // TODO Auto-generated method stub
      return null;
   }


   /**
    * Close.
    */
   @Override
   public void close()
   {
      // TODO Auto-generated method stub
      
   }


}
