package net.alantea.tablerwork.xlsx;

import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import net.alantea.tablerwork.TablerWorkCell;
import net.alantea.tablerwork.TablerWorkFile.Alignment;
import net.alantea.tablerwork.TablerWorkFile.Format;

public class XlsxWorkCell extends TablerWorkCell
{
   private XSSFCell cell;

   public XlsxWorkCell(XlsxWorkRow row, int num)
   {
      super(row, num);
      cell = row.getXSSFRow().getCell(num, MissingCellPolicy.CREATE_NULL_AS_BLANK);
   }

   public XlsxWorkCell(XlsxWorkRow row, String reference)
   {
      this(row, TablerWorkCell.getCellReferenceAsInt(reference));
   }

   @Override
   public int getIntegerValue()
   {
      return (int) getNumericValue();
   }

   @Override
   public double getNumericValue()
   {
      return getCellNumericValue(cell);
   }

   @Override
   public String getStringValue()
   {
      return getCellStringValue(cell);
   }

   @Override
   public String getHyperlink()
   {
      return getCellHyperlink(cell);
   }

   @Override
   public void setValue(String value)
   {
      setCellValue(cell, value);
   }

   @Override
   public void setValue(int value)
   {
      setCellValue(cell, value);
   }

   @Override
   public void setValue(double value)
   {
      setCellValue(cell, value);
   }

   @Override
   public void setHyperlink(String link)
   {
      setCellHyperlink(cell, link);
   }

   /**
    * Sets the cell format.
    *
    * @param format the format
    * @param alignment the alignment
    */
   public void setFormat( Format format, Alignment alignment)
   {
      setCellFormat(this.cell, format, alignment);
   }


   
   // =================================================================================================================
   
   
   public static double getCellNumericValue(XSSFCell xssfCell)
   {

      double ret = 0.0f;
      if (xssfCell == null)
      {
         return ret;
      }

      if (xssfCell.getCellStyle().getDataFormatString().contains("%"))
      {
         // detect percent values
         ret = xssfCell.getNumericCellValue();
      }
      else
      {
         // Get cell value
         switch (xssfCell.getCellType())
         {
            case _NONE:
            case BLANK:
            case ERROR:
            case BOOLEAN:
               ret = 0.0f;
               break;
            case NUMERIC:
               double value = xssfCell.getNumericCellValue();
               ret = value;
               break;
            case FORMULA:
               switch (xssfCell.getCachedFormulaResultType())
               {
                  case _NONE:
                  case BLANK:
                  case ERROR:
                  case FORMULA:
                  case BOOLEAN:
                     ret = 0.0f;
                     break;
                  case NUMERIC:
                     double value1 = xssfCell.getNumericCellValue();
                     ret = value1;
                     break;
                  case STRING:
                     String line = xssfCell.getRichStringCellValue().toString();
                     try
                     {
                        ret = Double.parseDouble(line);
                     }
                     catch (NumberFormatException ex)
                     {
                        ret = 0.0;
                     }
                     break;
               }
               break;
            default:
               String line = xssfCell.getStringCellValue().replaceAll(" ", "");
               try
               {
                  ret = Double.parseDouble(line);
               }
               catch (NumberFormatException ex)
               {
                  ret = 0.0;
               }
               break;
         }
      }
      return ret;
   }

   public static String getCellStringValue(XSSFCell xssfCell)
   {
      String ret = "";
      if (xssfCell == null)
      {
         return "";
      }

      // Get cell value
      switch (xssfCell.getCellType())
      {
         case _NONE:
         case BLANK:
         case ERROR:
         case FORMULA:
         case BOOLEAN:
            ret = "";
            break;
         case NUMERIC:
            double value = xssfCell.getNumericCellValue();
            ret = "" + value;
            break;
         default:
            ret = xssfCell.getStringCellValue();
            break;
      }
      ret = ret.replaceAll("[\n]", " ");
      return ret;
   }

   public static String getCellHyperlink(XSSFCell xssfCell)
   {
      XSSFHyperlink h = xssfCell.getHyperlink();
      return (h != null) ? h.getAddress() : null;
   }
   
   public static void setCellValue(XSSFCell xssfCell, String value)
   {
      xssfCell.setCellValue(value);
   }

   public static void setCellValue(XSSFCell xssfCell, int value)
   {
      xssfCell.setCellValue(value);
   }

   public static void setCellValue(XSSFCell xssfCell, double value)
   {
      xssfCell.setCellValue(value);
   }

   public static void setCellHyperlink(XSSFCell xssfCell, String link)
   {
      // TODO Auto-generated method stub

   }

   /**
    * Sets the cell format.
    *
    * @param xssfCell the xssf cell
    * @param format the format
    * @param alignment the alignment
    */
   public static void setCellFormat(XSSFCell xssfCell, Format format, Alignment alignment)
   {
      XSSFWorkbook workbook = xssfCell.getSheet().getWorkbook();
      
      XSSFCellStyle style = workbook.createCellStyle();
      style.setWrapText(true);
      
      switch(alignment)
      {
         case DEFAULT :
            break;
               
         case CENTER :
            style.setAlignment(HorizontalAlignment.CENTER);
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            break;
            
         case HORIZONTALCENTER :
            style.setAlignment(HorizontalAlignment.CENTER);
            break;

         
      case VERTICALCENTER :
         style.setVerticalAlignment(VerticalAlignment.CENTER);
         break;


         case TOP :
            style.setVerticalAlignment(VerticalAlignment.TOP);
            break;

         case BOTTOM :
            style.setVerticalAlignment(VerticalAlignment.TOP);
            break;

         case LEFT :
            style.setAlignment(HorizontalAlignment.LEFT);
            break;

         case RIGHT :
            style.setAlignment(HorizontalAlignment.RIGHT);
            break;

         case TOPLEFT :
            style.setVerticalAlignment(VerticalAlignment.TOP);
            style.setAlignment(HorizontalAlignment.LEFT);
            break;

         case TOPRIGHT :
            style.setVerticalAlignment(VerticalAlignment.TOP);
            style.setAlignment(HorizontalAlignment.RIGHT);
            break;

         case BOTTOMLEFT :
            style.setVerticalAlignment(VerticalAlignment.BOTTOM);
            style.setAlignment(HorizontalAlignment.LEFT);
            break;

         case BOTTOMRIGHT :
            style.setVerticalAlignment(VerticalAlignment.BOTTOM);
            style.setAlignment(HorizontalAlignment.RIGHT);
            break;
      }
      short styleFormat = workbook.createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(format.value));
      style.setDataFormat(styleFormat);
      
      xssfCell.setCellStyle(style);
   }
   
   /**
    * Fill a cell.
    *
    * @param xssfCell the xssf cell
    * @param element the element
    */
   static void fillCell(XSSFCell xssfCell, Object element)
   {
      if (element == null)
      {
         xssfCell.setCellValue("");
      }
      else if (element.getClass().equals(String.class))
      {
         xssfCell.setCellValue((String) element);
      }
      else if (element.getClass().equals(Double.class))
      {
         xssfCell.setCellValue((Double) element);
      }
      else if (element.getClass().equals(Integer.class))
      {
         xssfCell.setCellValue((Integer) element);
      }
   }
   
   public static void setCellStyle(XSSFCell xssfCell, XSSFCellStyle styleText)
   {
      xssfCell.setCellStyle(styleText);
   }
}
