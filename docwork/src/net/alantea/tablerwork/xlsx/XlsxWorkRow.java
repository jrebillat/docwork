package net.alantea.tablerwork.xlsx;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import net.alantea.tablerwork.TablerWorkCell;
import net.alantea.tablerwork.TablerWorkFile.Alignment;
import net.alantea.tablerwork.TablerWorkFile.Format;
import net.alantea.tablerwork.TablerWorkRow;

/**
 * The Class XlsxWorkRow.
 */
public class XlsxWorkRow extends TablerWorkRow
{
   /** The row. */
   private XSSFRow row;

   /**
    * Instantiates a new xlsx work row.
    *
    * @param sheet the sheet
    * @param rowNumber the row number
    */
   public XlsxWorkRow(XlsxWorkSheet sheet, int rowNumber)
   {
      super(sheet, rowNumber);
      this.row = sheet.getXSSFSheet().getRow(rowNumber);
      if (row == null)
         row = sheet.getXSSFSheet().createRow(rowNumber);
   }

   /**
    * Gets the cell.
    *
    * @param cellReference the cell reference
    * @return the cell
    */
   @Override
   public TablerWorkCell getCell(String cellReference)
   {
      return new XlsxWorkCell(this, TablerWorkCell.getCellReferenceAsInt(cellReference));
   }
   
   public XSSFCell getXSSFCell(int cellNumber)
   {
      return row.getCell(cellNumber);
   }
   
   public XSSFCell getXSSFCell(String cellReference)
   {
      return row.getCell(TablerWorkCell.getCellReferenceAsInt(cellReference));
   }

   /**
    * Gets the first cell num.
    *
    * @return the first cell num
    */
   @Override
   public int getFirstCellNum()
   {
      return row.getFirstCellNum();
   }

   /**
    * Gets the last cell num.
    *
    * @return the last cell num
    */
   @Override
   public int getLastCellNum()
   {
      return row.getLastCellNum();
   }

   /**
    * Gets the integer value.
    *
    * @param cellReference the cell reference
    * @return the integer value
    */
   @Override
   public int getIntegerValue(String cellReference)
   {
      return (int) XlsxWorkCell.getCellNumericValue(getXSSFCell(cellReference));
   }

   /**
    * Gets the numeric value.
    *
    * @param cellReference the cell reference
    * @return the numeric value
    */
   @Override
   public double getNumericValue(String cellReference)
   {
      return XlsxWorkCell.getCellNumericValue(getXSSFCell(cellReference));
   }

   /**
    * Gets the string value.
    *
    * @param cellReference the cell reference
    * @return the string value
    */
   @Override
   public String getStringValue(String cellReference)
   {
      return XlsxWorkCell.getCellStringValue(getXSSFCell(cellReference));
   }

   /**
    * Gets the hyperlink.
    *
    * @param cellReference the cell reference
    * @return the hyperlink
    */
   @Override
   public String getHyperlink(String cellReference)
   {
      return XlsxWorkCell.getCellHyperlink(getXSSFCell(cellReference));
   }

   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   @Override
   public void setValue(String cellReference, String value)
   {
      XlsxWorkCell.setCellValue(getXSSFCell(cellReference), value);
   }

   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   @Override
   public void setValue(String cellReference, int value)
   {
      XlsxWorkCell.setCellValue(getXSSFCell(cellReference), value);
   }

   /**
    * Sets the value.
    *
    * @param cellReference the cell reference
    * @param value the value
    */
   @Override
   public void setValue(String cellReference, double value)
   {
      XlsxWorkCell.setCellValue(getXSSFCell(cellReference), value);
   }

   /**
    * Sets the hyperlink.
    *
    * @param cellReference the cell reference
    * @param link the link
    */
   @Override
   public void setHyperlink(String cellReference, String link)
   {
      XlsxWorkCell.setCellHyperlink(getXSSFCell(cellReference), link);
   }

   /**
    * Gets the XSSF row.
    *
    * @return the XSSF row
    */
   public XSSFRow getXSSFRow()
   {
      return row;
   }

   /**
    * Sets content.
    *
    * @param map the map
    */
   public void setFollowingRowsContent(Map<String, List<List<Object>>> map)
   {
      XSSFRow xrow = this.row;
      XSSFSheet worksheet = xrow.getSheet();
      
      int rowNumStart = -1;
      int rowNum = -1;
      boolean firstRow = true;

      for ( String sector : map.keySet())
      {
         int sectorRowStart = 1;
         int sectorRowEnd = 1;
         boolean firstSectorRow = true;
         for (List<Object> data : map.get(sector))
         {
            String sectorToShow = sector;
            if (firstRow)
            {
               rowNumStart = xrow.getRowNum();
               rowNum = rowNumStart;
               firstRow = false;
            }
            else
            {
               worksheet.shiftRows(rowNum, worksheet.getLastRowNum(), 1);
               xrow = worksheet.createRow(rowNum++);
            }
            
            if (firstSectorRow)
            {
               sectorRowStart = xrow.getRowNum();
               sectorRowEnd = sectorRowStart;
               firstSectorRow = false;
            }
            else
            {
               sectorToShow = null;
               sectorRowEnd = xrow.getRowNum();
            }
            List<Object> completeList = new LinkedList<>(data);
            completeList.add(0, sectorToShow);
            setContent(xrow, firstRow, completeList.toArray());
         }
         
         if ((!firstRow) && (sectorRowStart != sectorRowEnd))
         {
            CellRangeAddress range = new CellRangeAddress(sectorRowStart, sectorRowEnd, 0, 0);
            worksheet.addMergedRegion(range);
            firstSectorRow = true;
         }
      }
      if (rowNumStart != -1)
      {
         getSheet().addCellBorders(rowNumStart, 0, rowNum, 5);
      }
   }

   /**
    * Sets content.
    *
    * @param elements the elements
    */
   public void setContent(Object... elements)
   {
      XSSFCellStyle styleText = row.getSheet().getWorkbook().createCellStyle();
      styleText.setWrapText(true);
      styleText.setVerticalAlignment(VerticalAlignment.CENTER);
      row.setRowStyle(styleText);
      
      int numcell = 0;
      Alignment currentAlignment = Alignment.DEFAULT;
      Format currentFormat = Format.PERCENTAGE;
      for( Object element : elements)
      {
         if (element instanceof Format)
         {
            currentFormat = (Format)element;
         }
         else if (element instanceof Alignment)
         {
            currentAlignment = (Alignment)element;
         }
         else
         {
            XSSFCell cell;
            cell = row.getCell(numcell++, MissingCellPolicy.CREATE_NULL_AS_BLANK);
         
            if (element instanceof String)
            {
               cell.setCellStyle(styleText);
            }
            if (element instanceof String)
            {
               XlsxWorkCell.setCellFormat(cell, Format.TEXT, currentAlignment);
            }
            else
            {
               XlsxWorkCell.setCellFormat(cell, currentFormat, currentAlignment);
            }
            XlsxWorkCell.fillCell(cell, element);
            currentFormat = Format.PERCENTAGE;
            currentAlignment = Alignment.TOPLEFT;
         }
      }
   }
}
