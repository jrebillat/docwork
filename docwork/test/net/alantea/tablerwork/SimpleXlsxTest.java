package net.alantea.tablerwork;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;


@TestMethodOrder(MethodOrderer.MethodName.class)
public class SimpleXlsxTest
{
   
   @Test
   public void T01_createSimpleFileTest()
   {
      TablerWorkFile file = TablerWorkFile.createTemporaryTablerWorkFile("TablerWorkTest", ".xlsx");
      Assertions.assertNotNull(file);
   }
   
   @Test
   public void T02_writeAndReadTest()
   {
      TablerWorkFile writefile = TablerWorkFile.createTemporaryTablerWorkFile("TablerWorkTest", ".xlsx");
      Assertions.assertNotNull(writefile);
      
      TablerWorkSheet writesheet = writefile.createSheet("Test");
      Assertions.assertNotNull(writesheet);
      
      TablerWorkRow writerow = writesheet.getRow(1);
      Assertions.assertNotNull(writerow);
      
      TablerWorkCell writecell = writerow.getCell(1);
      Assertions.assertNotNull(writecell);
      writecell.setValue("This is a test");
      
      writefile.save();
      writefile.close();
      
      String filePath = writefile.getFilePath();

      TablerWorkFile readfile = TablerWorkFile.loadTablerWorkFile(filePath);
      Assertions.assertNotNull(readfile);
      TablerWorkSheet readsheet = writefile.getSheet("Test");
      Assertions.assertNotNull(readsheet);
      TablerWorkRow readrow = readsheet.getRow(1);
      Assertions.assertNotNull(readrow);
      
      TablerWorkCell readcell = readrow.getCell(1);
      Assertions.assertNotNull(readcell);
      String readvalue = readcell.getStringValue();
      Assertions.assertNotNull(readvalue);
      Assertions.assertEquals("This is a test", readvalue);

      readfile.close();
   }
   
}
