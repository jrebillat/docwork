package net.alantea.docwork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import net.alantea.tools.zip.ZipInputStreamContainer;

public class TestDocx
{
   private static final Pattern TAG_REGEX = Pattern.compile("<w:t.*?>(.*?)</w:t>", Pattern.DOTALL);
   
   private static int count = 0;

   public static void main(String[] args) throws IOException
   {
      ZipInputStreamContainer cont = new ZipInputStreamContainer("C:/Temp/testdoc.docx");
      
      InputStream istream = cont.getFile("word/document.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

         stream.forEach(TestDocx::getText);
         
     bufreader.close();
     System.out.println("Count is " + count);
   }
   
   public static List<String> getText(String line)
   {
      Matcher matcher = TAG_REGEX.matcher(line);
      while (matcher.find())
      {
         count += matcher.group(1).length();
      }
      return null;
   }

}
