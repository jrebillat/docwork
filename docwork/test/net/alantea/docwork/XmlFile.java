/*
 * Copyright (C) 2021 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.alantea.docwork;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author favdb
 */
public class XmlFile {

   public static final String TT = "ZipXml";

   private final File file;
   private ZipFile zipFile;
   private Enumeration<? extends ZipEntry> entries;

   public static String getText(File f) {
      if (f.getAbsolutePath().endsWith("odt")) {
         return XmlFile.getText(f, "content.xml", "text:h, text:p");
      } else if (f.getAbsolutePath().endsWith("docx")) {
         return XmlFile.getText(f, "word/document.xml", "w:t");
      }
      return "";
   }

   public static String getText(File f, String docEntry, String tags) {
      return getText(f, docEntry, tags.split(","));
   }

   public static String getText(File file, String docEntry, String[] tags) {
      if (!file.exists()) {
         return "";
      }
      String s = "";
      try {
         XmlFile xml = new XmlFile(file);
         ZipEntry entry = xml.open(docEntry);
         if (entry == null) {
            System.out.println(TT + ".getText entry " + docEntry + " not exists");
         }
         s = xml.getContent(tags);
         xml.close();
      } catch (ParserConfigurationException | SAXException | IOException ex) {
         System.out.println(TT + ".getText(...)");
         ex.printStackTrace();
      }
      while (s.contains("  ")) {
         s = s.replace("  ", " ");
      }
      return s;
   }
   private Document document;

   public XmlFile(File file) {
      this.file = file;
   }

   public ZipEntry open(String entryName) throws IOException, SAXException, ParserConfigurationException {
      //App.trace(TT + ".zipOpen()");
      zipFile = new ZipFile(file);
      ZipEntry zipEntry = getEntry(entryName);
      if (zipEntry != null) {
         document = getDocument(zipEntry);
      }
      return zipEntry;
   }

   public ZipEntry getEntry(String entryName) {
      entries = zipFile.entries();
      while (entries.hasMoreElements()) {
         ZipEntry entry = entries.nextElement();
         if (entry.getName().equals(entryName)) {
            return entry;
         }
      }
      return null;
   }

   public void close() throws IOException {
      zipFile.close();
   }

   public Document getDocument(ZipEntry entry) throws ParserConfigurationException, SAXException, IOException {
      DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
      InputStream zinput = zipFile.getInputStream(entry);
      Document doc = documentBuilder.parse(zinput);
      if (doc == null) {
         return null;
      }
      return doc;
   }

   public NodeList getElements(String tag) {
      Element rootNode = document.getDocumentElement();
      return rootNode.getElementsByTagName(tag);
   }

   public String getContent(String tag) {
      Element rootNode = document.getDocumentElement();
      NodeList nodes = rootNode.getElementsByTagName(tag);
      if (nodes.getLength() < 1) {
         return "";
      }
      List<String> b = new ArrayList<>();
      for (int i = 0; i < nodes.getLength(); i++) {
         Node el = nodes.item(i);
         b.add(el.getTextContent());
      }
      return join(b, " ");
   }

   public String getContent(String[] tags) {
      List<String> b = new ArrayList<>();
      for (String tag : tags) {
         b.add(getContent(tag.trim()));
      }
      return checkPunctuation(join(b, " "));
   }
   
   private static String join(List<String> array, String separator) {
      String sep = separator;
      if (separator == null) {
         sep = "";
      }
      final StringBuilder buf = new StringBuilder();
      if (array != null) {
         for (int i = 0; i < array.size(); i++) {
            if (i > 0) {
               buf.append(sep);
            }
            if (array.get(i) != null) {
               buf.append((String) array.get(i));
            }
         }
      }
      return buf.toString();
   }

   public static String checkPunctuation(String str) {
      String r = str;
      String punc = ",.;:-_\t?!%";
      for (int i = 0; i < punc.length(); i++) {
         r = r.replace(" " + punc.charAt(i) + " ", punc.charAt(i) + " ");
      }
      return r;
   }

   public List<Element> getManyElements(String... tags) {
      //App.trace(TT + ".getManyElements(tags=" + Arrays.asList(tags) + ")");
      List<Element> nodes = new ArrayList<>();
      List<String> list = Arrays.asList(tags);
      Element rootNode = document.getDocumentElement();
      NodeList textNodes = rootNode.getElementsByTagName("office:text");
      Node node = (textNodes.item(0)).getFirstChild();
      while (node != null) {
         if (list.contains(node.getNodeName())) {
            nodes.add((Element) node);
         }
         node = node.getNextSibling();
      }
      return nodes;
   }

}
