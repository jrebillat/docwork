package net.alantea.docwork;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import net.alantea.tools.xml.XMLParser;
import net.alantea.tools.zip.ZipInputStreamContainer;

public class Test2
{
   static String content = "";
   private static String filecontent = "";

   public static void main(String[] args) throws IOException
   {
      ZipInputStreamContainer cont = new ZipInputStreamContainer("C:/Temp/testdoc.odt");

      InputStream istream = cont.getFile("content.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      boolean ok = true;
      while (ok)
      {
         String l = bufreader.readLine();
         if (l == null)
         {
            ok = false;
         }
         else
         {
            filecontent += l;
         }
      }
      bufreader.close();

      int n = 0;
      System.out.println(filecontent.length());
      while(filecontent.contains("Policepard"))
      {
         int k = filecontent.indexOf("Policepard");
         System.out.println(k);
         filecontent = filecontent.substring(0, k + 9) + filecontent.substring(k + 11);
      }
      while ((n + 42370) < filecontent.length())
      {
         System.out.println(filecontent.substring(n, n + 42370));
         n += 42370;
      }
      OOParser parser = new OOParser();
      parser.load(filecontent );
      
      System.out.println(content);
      System.out.println(content.length());
   }
   
//   private static void getFileContent(String line)
//   {
//      filecontent += line;
//   }
   
   static class OOParser extends XMLParser
   {
      
      @SuppressWarnings("unused")
      private void text_pEnd()
      {
         content += getCurrentText();
      }
      
      @SuppressWarnings("unused")
      private void text_spanEnd()
      {
         content += getCurrentText();
      }
   }

}
