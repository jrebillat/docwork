package net.alantea.docwork;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import net.alantea.tools.zip.ZipInputStreamContainer;

public class Test1
{
   private static final Pattern TAG_OO_REGEX = Pattern.compile("<text:p.*?>(.*?)</text:p>", Pattern.DOTALL);
   private static final Pattern TAG_WORD_REGEX = Pattern.compile("<w:t.*?>(.*?)</w:t>", Pattern.DOTALL);
   
   private static int wcount = 0;
   private static int oocount = 0;

   public static void main(String[] args) throws IOException
   {
//       int ooc = getOOCount("C:/Temp/testdoc.odt");
//       int wc = getWordCount("C:/Temp/testdoc.docx");
      String txt1 = XmlFile.getText(new File("C:/Temp/testdoc.odt"));
      System.out.println(txt1);
      String txt2 = XmlFile.getText(new File("C:/Temp/testdoc.docx"));
      System.out.println(txt1);
      System.out.println("---------------------------------------");
      System.out.println(txt2);
       System.out.println("OO : " + txt1.length() + ", Word : " + txt2.length());
   }

   public static int getOOCount(String path) throws IOException
   {
      oocount = 0;
      ZipInputStreamContainer cont = new ZipInputStreamContainer(path);

      InputStream istream = cont.getFile("content.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

      stream.forEach(Test1::getOOText);

      bufreader.close();
      return oocount;
   }
   
   /**
    * Gets the OO text.
    *
    * @param line the line
    * @return the OO text
    */
   public static List<String> getOOText(String line)
   {
      line = line.replaceAll("&\\S+;", ".");
      Matcher matcher = TAG_OO_REGEX.matcher(line);
      while (matcher.find())
      {
         String found = matcher.group(1);
         found = found.replaceAll("<text:span.*?>", "");
         found = found.replaceAll("</text:span>", "");
         found = found.replaceAll("<text:s/>", " ");
         found = found.replaceAll("<text:soft-page-break/>", "");
         found = found.replaceAll("<text:bookmark-start.*?/>", "");
         found = found.replaceAll("<text:bookmark-end.*?/>", "");
         found = found.replaceAll("<text:p.*?>", "");
         found = found.replaceAll("\\?\\?", " ?");
         found = found.replaceAll("\\?!", " !");
         oocount += found.length();
      }
      return null;
   }

   public static int getWordCount(String path) throws IOException
   {
      wcount = 0;
      ZipInputStreamContainer cont = new ZipInputStreamContainer(path);
      
      InputStream istream = cont.getFile("word/document.xml");
      Reader reader = new InputStreamReader(istream, "UTF-8");
      BufferedReader bufreader = new BufferedReader(reader);
      Stream<String> stream = bufreader.lines();

      stream.forEach(Test1::getWordText);
         
     bufreader.close();
     return wcount;
   }
   
   public static List<String> getWordText(String line)
   {
      System.out.println(line);
      String[] sublines = line.split("<w:pageBreakBefore/>");
      for (String subline : sublines)
      {
         Matcher matcher = TAG_WORD_REGEX.matcher(subline);
         //String[] slines = subline.split("<w:pageBreakBefore/>");
         System.out.println("\n-----------------------------------\n");
         while (matcher.find())
         {
            wcount += matcher.group(1).length();
            System.out.print(matcher.group(1));
         }
      }
      return null;
   }
}
